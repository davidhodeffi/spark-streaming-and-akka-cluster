package com.davidh.streaming

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.Try
import collection.mutable.Stack
import org.scalatest._

import scala.collection.mutable

class MainTest extends FlatSpec with Matchers with BeforeAndAfterAll {


  def initTables() ={
    SparkTest.spark.sql("drop table if exists words")
    SparkTest.spark.sql("drop table if exists events")
    SparkTest.spark.sql("create table words(word string, value int)")
    SparkTest.spark.sql("create table events(event_type string, value int)")
  }
  "A Spark job" should "show statistics" in {
    val statistics = Main.createEmptyStatistics(SparkTest.spark)
    statistics.print()

  }
  "A Spark job" should "aggregate statistics" in {
    initTables()
    val queue = mutable.Queue[RDD[String]]()

    val stream = SparkTest.ssc.queueStream(queue)
//    val stream = SparkTest.ssc.textFileStream("file:///Users/davidh/workspace/spark-streaming/stream_data")
//    stream.print()
    val statistics = Main.createEmptyStatistics(SparkTest.spark)
    Main.analayzeStream(SparkTest.spark,SparkTest.ssc,stream)
//    statistics.print()(SparkTest.spark)
    SparkTest.ssc.start()
    for (i <- 1 to 2 ){
      queue.synchronized{
        val data = SparkTest.spark.sparkContext.textFile("stream_data/stream.txt")
        queue +=  data
//        statistics.print()(SparkTest.spark)

      }
    }
    SparkTest.ssc.awaitTermination()

  }



  "A Json" should "marshal string to event" in {
    JsonUtil.fromJson("{ \"event_type\": \"baz\", \"data\": \"amet\", \"timestamp\": 1536493424 }")
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[Int]
    a [NoSuchElementException] should be thrownBy {
      emptyStack.pop()
    }
  }
}

object SparkTest{
  val spark = SparkSession
    .builder().enableHiveSupport()
    .appName("Event Windowing")
    .master("local[2]")
    .getOrCreate()
  val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
}