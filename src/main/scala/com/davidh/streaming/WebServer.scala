package com.davidh.streaming

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.davidh.streaming.WebServer.system
import spray.json.DefaultJsonProtocol

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn


trait  StatisticsDatabase {
  def getEvents:Map[String,Long]
  def getWords:Map[String,Long]
}


trait ServerRoute extends SprayJsonSupport with DefaultJsonProtocol{
  val databse:StatisticsDatabase
  val route: Route =
    path("events") {
      get {
        complete(databse.getEvents)
      }
    } ~ path("words") {
      get {
        complete(databse.getWords)
      }
    }

}
class WebServer(val databse:StatisticsDatabase)(
                implicit val system: ActorSystem,
                implicit val materializer: ActorMaterializer,
                implicit val executionContext: ExecutionContextExecutor) extends ServerRoute{

  def start() {


    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
object  WebServer extends SprayJsonSupport with DefaultJsonProtocol{
  implicit val system: ActorSystem = ActorSystem("my-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  def createServer(databse:StatisticsDatabase) ={
    new WebServer(databse)
  }

}

