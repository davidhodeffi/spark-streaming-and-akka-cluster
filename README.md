# Spark Streaming and Akka http

In this project we aggreagete events that are created by a generator.

Technology: 
Akka-HTTP as the rest server (port 8080)
/events 
return the count of occurence by event type
/words 
return the count of words from the 'data' field.


Analytics Of Data stream: 
Spark streaming is connecting to a socket on port  9999 and aggregate the events. events are aggregated each second and stored on Hive tables.

# Instructions 
1. create a socket that generate events on port 9999
```bash
	./generator-macosx-amd64 | nc -l  9999
```

2. run the app.
This can be done using IDE i.e. intellij community addition.  or using sbt to run Main class. 
on production it should be deployed to spark cluster using spark-submit (further work)

3. query the data using curl 
```bash
	curl -X GET http://localhost:8080/events
	//OUTPUT: {"bar":310,"foo":268,"baz":254}%
	curl -X GET http://localhost:8080/words
```

# Things to improve: 
1. code quality 
2. create configuration and cli.
3. update statistics incrementally. right now it is recalculated. when data would be huge, rest client would have long response time.
4. partition the data by hour/day/month/year
