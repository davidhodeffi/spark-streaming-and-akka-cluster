package com.davidh.streaming

import org.scalatest._
import akka.http.scaladsl.server._
import Directives._
import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.testkit.ScalatestRouteTest


class WebserverTest extends FlatSpec with Matchers with BeforeAndAfterAll with ServerRoute with ScalatestRouteTest{

  "A web server" should "return events" in {
    // tests:
    Get("/events") ~> route ~> check {
      responseAs[Map[String, Int]] shouldEqual Map("event1"->1,"event2" -> 2)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[String] should ===(""""{"event1":1,"event2":2}"""")
    }
  }

  it should "return words" in {
    // tests:
    Get("/words") ~> route ~> check {
      responseAs[Map[String, Int]] shouldEqual Map("hello"->1,"world" -> 2)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[String] should ===("""{"hello":1,"world":2}""")
    }
  }

  override val databse: StatisticsDatabase = new StatisticsDatabase {
    override def getEvents: Map[String, Int] = Map("event1"->1,"event2" -> 2)

    override def getWords: Map[String, Int] = Map("hello"->1,"world" -> 2)
  }
}
