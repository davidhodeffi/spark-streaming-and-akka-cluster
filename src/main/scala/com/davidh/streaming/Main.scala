package com.davidh.streaming

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import spray.json.DefaultJsonProtocol

import scala.util.Try

object Main {
  def main(args: Array[String]) = {

    val spark = SparkSession
      .builder().enableHiveSupport()
      .appName("Event Windowing")
      .master("local[*]")
      .getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
    spark.sql("create table if not exists words(word string, value int)")
    spark.sql("create table if not exists events(event_type string, value int)")
    val stream: ReceiverInputDStream[String] = ssc.socketTextStream("localhost", 9999)
    val statistics = createEmptyStatistics(spark)
    analayzeStream(spark, ssc, stream)
    val server = WebServer.createServer(new StatisticsDatabase {
      override def getEvents: Map[String, Long] = {
        val tuples = statistics.getEvents.map(row => (row.getString(0), row.getLong(1)))
        tuples.toMap
      }

      override def getWords: Map[String, Long] = {
        val tuples = statistics.getWords.map(row => (row.getString(0), row.getLong(1)))
        tuples.toMap
      }
    })

    stream.start()
    ssc.start()
    server.start()
    //stop
    stream.stop()
    ssc.awaitTermination()
  }

  def createEmptyStatistics(spark: SparkSession): Statistics = {
    Statistics(spark)
  }

  def analayzeStream(spark: SparkSession, ssc: StreamingContext, stream: DStream[String]) = {
    val onlySuccessEvents = stream.map(JsonUtil.fromJson).filter(_.isSuccess).map(_.get)
    val countByEventType = onlySuccessEvents.map(event => (event.event_type, 1)).reduceByKey(_ + _)
    val wordCount = onlySuccessEvents.flatMap(_.data.split(" ").map(word => (word, 1))).reduceByKey(_ + _)
    import spark.implicits._

    countByEventType.foreachRDD { rdd =>
      val streamStatistic = rdd.toDF("event_type", "value")
      streamStatistic.write.insertInto("events")
      streamStatistic.show()

    }

    wordCount.foreachRDD { rdd =>
      val streamStatistic = rdd.toDF("word", "value")
      streamStatistic.write.insertInto("words")
      streamStatistic.show()

    }

  }
}

case class Statistics(spark:SparkSession) {
  def getEvents = {
    spark.sql("select event_type,sum(value) from events group by event_type").collect()
  }

  def getWords = {
    spark.sql("select word,sum(value) from words group by word").collect()
  }
  def print() = {
    val events = spark.sql("select event_type,sum(value) from events group by event_type").show()
    val words = spark.sql("select word,sum(value) from words group by word").show()
  }
}


case class Event(
                  event_type: String,
                  data: String,
                  timestamp: Double
                )


object JsonUtil {

  import org.json4s._
  import org.json4s.jackson.JsonMethods._

  implicit val formats = DefaultFormats

  def fromJson(json: String): Try[Event] = {
    Try(parse(json).extract[Event])
  }
}
